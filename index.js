import express from "express";
import { v4 as uuidv4 } from "uuid";

const app = express();

app.use(express.json());

//Formatar a data e hora
const zeroFill = (n) => {
  return n < 9 ? `0${n}` : `${n}`;
};
const FormatData = (date) => {
  const d = zeroFill(date.getDate());
  const mo = zeroFill(date.getMonth() + 1);
  const y = zeroFill(date.getFullYear());
  const h = zeroFill(date.getHours());
  const mi = zeroFill(date.getMinutes());
  const s = zeroFill(date.getSeconds());
  const ml = zeroFill(date.getMilliseconds());

  return `${y}-${mo}-${d}T${h}:${mi}:${s}:${ml}Z`;
};
const date = new Date();
const dateActive = FormatData(date).toString();

const cpfMiddleware = (req, res, next) => {
  const { cpf } = req.params;
  let searchCpf = DB.find((item) => item.cpf === cpf);
  if (!searchCpf) {
    res.json({ error: "invalid cpf - user is not registered" });
  }
  next();
};

let DB = [];

app.post("/users", (req, res) => {
  const data = req.body;
  data["notes"] = [];
  data["id"] = uuidv4();
  DB.push(data);
  let output = { message: "User is updated" };
  output["users"] = DB;
  res.status(201).json(output);
});

app.post("/users/:cpf/notes", cpfMiddleware, (req, res) => {
  const { cpf } = req.params;
  const data = req.body;
  data["id"] = uuidv4();
  data["created_at"] = dateActive;
  let userNotes = DB.find((item) => item.cpf === cpf);
  userNotes["notes"].push(data);

  res
    .status(201)
    .json({ message: `${data.title} was added into ${userNotes.name} notes` });
  id += 1;
});

app.get("/users", (req, res) => {
  const data = DB;
  res.json(data);
});

app.patch("/users/:cpf(\\d+)", cpfMiddleware, (req, res) => {
  const { cpf } = req.params;
  const data = req.body;
  let userPatch = DB.find((item) => item.cpf === cpf);
  userPatch["name"] = data["name"];
  let newDB = DB.filter((item) => item.cpf != cpf);
  newDB.push(userPatch);
  DB = newDB;

  res.json(userPatch);
});

app.patch("/users/:cpf/notes/:id", cpfMiddleware, (req, res) => {
  const { cpf, id } = req.params;
  const data = req.body;

  let userNotes = DB.find((item) => item.cpf === cpf);
  let notes = userNotes.notes.find((item) => item.id === id);
  console.log(notes);
  notes["title"] = data.title;
  notes["content"] = data.content;
  notes["updated_at"] = dateActive;
  let newDB = DB.filter((item) => item.cpf != cpf);
  newDB.push(userNotes);
  DB = newDB;

  res
    .status(201)
    .json({ message: `${data.title} was added into ${userNotes.name} notes` });
  id += 1;
});

app.delete("/users/:cpf(\\d+)", cpfMiddleware, (req, res) => {
  const { cpf } = req.params;
  let userDelete = DB.find((item) => item.cpf === cpf);
  let newDB = DB.filter((item) => item.cpf != cpf);
  DB = newDB;

  res.json({
    message: "User is deleted",
    users: [],
  });
});

app.delete("/users/:cpf/notes/:id", cpfMiddleware, (req, res) => {
  const { cpf, id } = req.params;
  let userDeleteNotes = DB.find((item) => item.cpf === cpf);
  let newNotes = userDeleteNotes.notes.filter((item) => item.id != id);
  userDeleteNotes.notes = newNotes;
  let newDB = DB.filter((item) => item.cpf != cpf);
  newDB.push(userDeleteNotes);
  DB = newDB;

  res.json([]);
});

app.listen(3000, () => {
  console.log("Running at http://localhost:3000");
});
